### Setup

This file describes the procedure to set up the development environment for the 
"Ingenieure ohne Grenzen" (Engineers without Borders) Austria Competence Database project.

#### Prerequisites for the container environment

Docker is used to create an environment that can be built up without needing to install 
all of the additional software locally. 
To build the project docker >=1.10 and docker-compose >= 1.10 are needed.
For Windows or Mac OS the Docker Toolbox can be installed, which includes both docker and docker-compose. 

Information on how to install docker can be found [here](https://www.docker.com/products/docker). 

#### Building of development environment

1. First the project files need to be cloned from gitlab to your local filesystem

	`git clone https://gitlab.com/iog-austria/competence-db-docker.git`
	
2. Build the container for the project with docker-compose
	
	`docker-compose build`

3. Start and update the container

	`docker-compose up -d`

4. Check `localhost:8000` in your browser to find the current state of the webapp

If the website doesn't show up immediatly run `docker-compose up -d` again

### Entry into container

to enter a bash inside the running docker container with the django app run

`sudo docker exec -i -t django /bin/bash`

on a Linux system, or on the docker virtual machine.
From within the docker container usual django functions, like making migrations, can be accessed.
To display changes in the browser run 

`docker-compose restart` or run `docker-compose up -d` again.


