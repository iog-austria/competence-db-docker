### Usage of GIT
**use the following commands in git bash or linux bash**

to check out repository for the first time enter command
`git clone https://gitlab.com/iog-austria/competence-db-docker.git`

to see the current state of git in the current directory, you have to change
into the git directory using `cd filepath/to/git/directory`

#### 0) Check Status

inside the git directory use `git status` to see which files have been
changed or are untracked.

#### 1) Staging

to stage files to be commited enter `git add filename`

#### 2) Commiting

once all desired changes have been added use `git commit -m "commit message"`
to create a commit from the changes that have been made. the commit message
should describe which changes have been made or which new files have been added
to help find the changes later when looking for errors or changes.

#### 3) Pushing to the server

to upload the local commits to the git repository on the server enter `git push`


steps 1, 2 and 3 should be repeated after every series of changes to the project.
**before starting to work on the project**, use `git pull` to make sure that the
project is at the current status