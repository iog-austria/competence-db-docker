# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-09 15:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0009_auto_20170408_1254'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='focusgroup',
            name='focus_group',
        ),
        migrations.RemoveField(
            model_name='projectgroup',
            name='project_group',
        ),
        migrations.RemoveField(
            model_name='regionalgroup',
            name='regional_group',
        ),
        migrations.AddField(
            model_name='focusgroup',
            name='name',
            field=models.CharField(choices=[('IT', 'AG IT'), ('PR', 'AG PR')], default='IT', max_length=40),
        ),
        migrations.AddField(
            model_name='projectgroup',
            name='name',
            field=models.CharField(choices=[('TT', 'PG Tailoring Togo'), ('FH', 'PG Flüchtlingshilfe'), ('VR', 'PG Vortragsreihe')], default='TT', max_length=40),
        ),
        migrations.AddField(
            model_name='regionalgroup',
            name='name',
            field=models.CharField(choices=[('GRAZ', 'RG Graz'), ('SBGOÖ', 'RG Salzburg/Oberösterreich'), ('WIEN', 'RG Wien'), ('TIROL', 'RG Innsbruck'), ('LEOBEN', 'RG Leoben')], default='WIEN', max_length=40),
        ),
    ]
