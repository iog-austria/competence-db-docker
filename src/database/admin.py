from django.contrib import admin
from database.models import Member, RegionalGroup, FocusGroup, ProjectGroup


class MemberAdmin(admin.ModelAdmin):
    model = Member


class RegionalGroupAdmin(admin.ModelAdmin):
    model = RegionalGroup


class FocusGroupAdmin(admin.ModelAdmin):
    model = FocusGroup


class ProjectGroupAdmin(admin.ModelAdmin):
    model = ProjectGroup


admin.site.register(Member, MemberAdmin)
admin.site.register(RegionalGroup, RegionalGroupAdmin)
admin.site.register(FocusGroup, FocusGroupAdmin)
admin.site.register(ProjectGroup, ProjectGroupAdmin)
