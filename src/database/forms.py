from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
# from django.core.validators import validate_email
# from django.core.exceptions import ValidationError


class MemberRegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

    technical_competences = forms.CharField(required=True)
    languages = forms.CharField(required=False)
    other_competences = forms.CharField(required=False)

    iog_member = forms.BooleanField(required=False)
    contact_person = forms.CharField()

    telephone_number = forms.CharField(required=False)

    address_street = forms.CharField(required=False)
    address_street_number = forms.CharField(required=False)
    address_plz = forms.CharField(required=False, max_length=5)
    address_city = forms.CharField(required=False, max_length=30)
    address_country = forms.CharField(required=False, max_length=30)

    regional_group = forms.CharField(required=False)
    focus_group = forms.CharField(required=False)
    project_group = forms.CharField(required=False)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'technical_competences',
                  'languages', 'other_competences', 'iog_member',
                  'contact_person', 'telephone_number', 'address_street',
                  'address_street_number', 'address_plz',
                  'address_city', 'address_country',
                  'regional_group', 'focus_group', 'project_group')

    def save(self, commit=True):
        member = super(MemberRegistrationForm, self).save(commit=False)
        member.email = self.cleaned_data['email']
        member.first_name = self.cleaned_data['first_name']
        member.last_name = self.cleaned_data['last_name']

        member.technical_competences = self.cleaned_data[
            'technical_competences']
        member.languages = self.cleaned_data['languages']
        member.other_competences = self.cleaned_data['other_competences']

        member.iog_member = self.cleaned_data['iog_member']
        member.contact_person = self.cleaned_data['contact_person']

        member.telephone_number = self.cleaned_data['telephone_number']

        member.address_street = self.cleaned_data['address_street']
        member.address_street_number = self.cleaned_data[
            'address_street_number']
        member.address_plz = self.cleaned_data['address_plz']
        member.address_city = self.cleaned_data['address_city']
        member.address_country = self.cleaned_data['address_country']

        member.regional_group = self.cleaned_data['regional_group']
        member.focus_group = self.cleaned_data['focus_group']
        member.project_group = self.cleaned_data['project_group']

        # try:
        #     validate_email(self.email)
        # except ValidationError:
        #     raise ValidationError("Not a valid Email!")

        if commit:
            member.save()

        return member
