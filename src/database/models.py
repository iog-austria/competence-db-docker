from django.db import models
# from django.core.validators import validate_email


class Member(models.Model):
    first_name = models.CharField(max_length=50, blank=False, default="")
    last_name = models.CharField(max_length=50, blank=False, default="")
    email = models.EmailField(unique=True, blank=False, max_length=100,
                              default="")
    # validators=validate_email) Einbauen bei POST!
    telephone_number = models.CharField(max_length=50, default="")
    address_street = models.CharField(max_length=80, default="")
    address_street_number = models.CharField(max_length=20, default="")
    address_plz = models.CharField(max_length=5, default="")
    address_city = models.CharField(max_length=30, default="")
    address_country = models.CharField(max_length=30, default="")


    regional_group = models.ForeignKey('RegionalGroup',
                                       on_delete=models.CASCADE,
                                       default="")

    focus_group = models.ForeignKey('FocusGroup',
                                    on_delete=models.CASCADE,
                                    default="")

    project_group = models.ForeignKey('ProjectGroup',
                                      on_delete=models.CASCADE,
                                      default="")

    iog_member = models.BooleanField
    contact_person = models.ForeignKey('self', default="")

    technical_competences = models.TextField(max_length=500, default="")
    languages = models.TextField(max_length=500, default="")
    other_competences = models.TextField(max_length=500, default="")

    def __str__(self):
        return '{fn} {ln}'.format(fn = self.first_name, ln = self.last_name)


class RegionalGroup(models.Model):
    # REGIONAL_GROUP_CHOICES = (
    #     ('GRAZ', 'RG Graz'),
    #     ('SBGOÖ', 'RG Salzburg/Oberösterreich'),
    #     ('WIEN', 'RG Wien'),
    #     ('TIROL', 'RG Innsbruck'),
    #     ('LEOBEN', 'RG Leoben')
    # )

    regional_group = models.CharField(max_length=50, default="")

    # def __str__(self):
    #     return self.get_regional_group_display()


class FocusGroup(models.Model):
    # FOCUS_GROUP_CHOICES = (
    #     ('IT', 'AG IT'),
    #     ('FR', 'AG Fundraising'),
    #     ('PR', 'AG PR')
    # )

    focus_group = models.CharField(max_length=50, default="")

    # def __str__(self):
    #     return self.get_focus_group_display()


class ProjectGroup(models.Model):
     # PROJECT_GROUP_CHOICES = (
     #     ('TT', 'PG Tailoring Togo'),
     #     ('FH', 'PG Flüchtlingshilfe'),
     #     ('VR', 'PG Vortragsreihe')
     # )

     project_group = models.CharField(max_length=50, default="")

     # def __str__(self):
     #     return self.get_project_group_display()